package com.calculator.commandsForCalculation;

import com.calculator.Command;

import java.util.HashMap;
import java.util.Stack;
public class Comment implements Command {
    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        for (String word : arguments ) {
            System.out.print(word+" ");
        }
        System.out.println("");
    }
}
