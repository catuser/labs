package com.calculator.commandsForCalculation;

import com.calculator.Command;

import java.util.HashMap;
import java.util.Stack;
public class Define implements Command {
    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        try {
            myDefined.put(arguments[1],Double.valueOf(arguments[2]));
        }
        catch (ArrayIndexOutOfBoundsException exception){
            System.err.println("failed to put arguments");
        }
    }
}
