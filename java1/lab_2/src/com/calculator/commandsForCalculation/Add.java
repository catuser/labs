package com.calculator.commandsForCalculation;

import com.calculator.Command;

import java.util.HashMap;
import java.util.Stack;
public class Add implements Command {
    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        if (myValue.size()>1)
            myValue.push(myValue.pop()+myValue.pop());
    }
}