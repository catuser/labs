package com.calculator.commandsForCalculation;

import com.calculator.Command;

import java.util.HashMap;
import java.util.Stack;

public class Print implements Command {

    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        if (myValue.size() != 0)
            System.out.println(myValue.peek());
        else {
            System.out.println("stack is empty");
        }
    }
}
