package com.calculator.commandsForCalculation;

import com.calculator.Command;

import java.util.HashMap;
import java.util.Stack;

public class Pop implements Command {

    public void todo(Stack<Double> myValue, HashMap<String, Double> myDefined, String[] arguments) {
        try {
            myValue.pop();
        }catch (java.util.EmptyStackException e) {
            System.err.println("stack is empty");
        }
    }
}
