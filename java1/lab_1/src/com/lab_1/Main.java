package com.lab_1;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("invalid args: ... input output");
            return;
        }

        try (Reader reader = new InputStreamReader(new FileInputStream(args[0]));
             Writer writer = new PrintWriter(args[1])) {
            new CSVSort(reader, writer, ';').groupByWordAndCount();
        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

}
