package com.lab_1;

import java.io.*;
import java.util.HashMap;

public class FrequencyMapParser {
    private BufferedReader reader;
    private int wordsNumber = 0;

    public FrequencyMapParser(Reader reader) {
        this.reader = new BufferedReader(reader);
    }

    public HashMap<String, Integer> parse() throws IOException {
        HashMap<String, Integer> frequencyMap = new HashMap<>();
        StringBuilder stringBuilder = new StringBuilder();
        int c;
        while ((c = reader.read()) != -1) {
                if (Character.isLetterOrDigit(c))
                    stringBuilder.append(Character.toChars(c));
                else if (stringBuilder.length() > 0) {
                    wordsNumber++;
                    String word = stringBuilder.toString();
                    frequencyMap.merge(word, 1, (value, newValue)->newValue = value + 1);
                    stringBuilder.setLength(0);
                }
        }

        if (stringBuilder.length() > 0) {
            wordsNumber++;
            String word = stringBuilder.toString();
            frequencyMap.merge(word, 1, (value, newValue)->newValue = value + 1);
            stringBuilder.setLength(0);
        }

        return frequencyMap;
    }

    public int getWordsNumber(){
        return wordsNumber;
    }

}
