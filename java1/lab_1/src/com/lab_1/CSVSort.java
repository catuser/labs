package com.lab_1;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

public class CSVSort {
    private Reader reader;
    private Writer writer;
    private char del;

    public CSVSort(Reader reader, Writer writer, char del) {
        this.reader = reader;
        this.writer = writer;
        this.del = del;
    }

    public void groupByWordAndCount() throws IOException {
            FrequencyMapParser parser = new FrequencyMapParser(reader);
            List<Map.Entry<String, Integer>> sortedList = Sort.asSortedList(parser.parse());
            int wordsNumber = parser.getWordsNumber();
            DecimalFormat formatter = new DecimalFormat();
            for (Map.Entry<String, Integer> entry : sortedList) {
                writer.append(entry.getKey())
                        .append(del)
                        .append(entry.getValue().toString())
                        .append(del)
                        .append(formatter.format(entry.getValue() / (float) wordsNumber * 100))
                        .append('\n');
            }
    }
}
