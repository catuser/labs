package com.lab_1;

import java.util.*;

public class Sort {
    public static List<Map.Entry<String, Integer>> asSortedList(Map<String, Integer> frequencyMap) {
        List<Map.Entry<String, Integer>> list = new ArrayList<>(frequencyMap.entrySet());
        list.sort(Comparator.comparingInt(Map.Entry<String, Integer>::getValue).reversed());

        return list;
    }
}
