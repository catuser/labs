package com.tetris.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

public class Utils {
    public static Image getScaledImage(String path, int width, int height) {
        Image img = null;
        try {
            img = ImageIO.read(Utils.class.getResource(path));
            img = img.getScaledInstance(width, height, 0);
        } catch (IOException e){
            System.err.println(e.getLocalizedMessage());
        }

        return img;
    }


}
