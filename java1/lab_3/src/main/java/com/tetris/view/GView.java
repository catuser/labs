package com.tetris.view;

import com.tetris.controller.Controller;
import com.tetris.Settings;
import com.tetris.view.gui.MainPanel;
import com.tetris.model.GameLogic;

import javax.swing.*;
import java.awt.*;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class GView implements View {
    private MainPanel mainPanel;
    private Timer timer;

    public GView(Controller controller, GameLogic gameLogic){
        javax.swing.SwingUtilities.invokeLater(() -> {
            mainPanel = new MainPanel(controller, gameLogic);
            JFrame frame  = new JFrame();
            frame.setTitle("Tetris");
            frame.setResizable(false);
            frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
            frame.add(mainPanel);
            frame.pack();
            frame.setVisible(true);
        });
    }

    @Override
    public void startNewGame() {
        mainPanel.startNewGame();
        timer.restart();
    }

    @Override
    public void reset() {
        mainPanel.reset();
        mainPanel.sync();
    }

    @Override
    public void showHighScores() {
        mainPanel.showHighScores();
        timer.stop();
    }

    @Override
    public void exit() {

    }

    @Override
    public void showAbout() {
        mainPanel.showAbout();
    }

    @Override
    public void updateLoop() {
        timer = new Timer(30, e -> mainPanel.sync());
        timer.setInitialDelay(0);
        timer.start();
    }
}
