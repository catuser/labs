package com.tetris.view.gui;

import javax.swing.*;
import java.awt.*;

public class LabelPanel extends JPanel {
    private JLabel infoLabel, timeLabel, scoreLabel;

    public LabelPanel(){
        super(new BorderLayout());
        setBorder(BorderFactory.createLineBorder(Color.black, 10));

        infoLabel = new JLabel("<html><u>Press \"Enter\" to start");
        Font font = new Font(infoLabel.getFont().getName(), Font.PLAIN, 22);
        infoLabel.setFont(font);
        infoLabel.setHorizontalAlignment(JLabel.CENTER);

        scoreLabel = new JLabel();
        scoreLabel.setFont(font);
        scoreLabel.setHorizontalAlignment(JLabel.RIGHT);
        scoreLabel.setPreferredSize(new Dimension(120, 50));

        timeLabel = new JLabel();
        timeLabel.setFont(font);
        timeLabel.setHorizontalAlignment(JLabel.LEFT);
        timeLabel.setPreferredSize(new Dimension(120, 50));

        add(infoLabel, BorderLayout.NORTH);
        add(timeLabel, BorderLayout.WEST);
        add(scoreLabel, BorderLayout.EAST);
    }

    public JLabel getTimeLabel() {
        return timeLabel;
    }

    public JLabel getScoreLabel() {
        return scoreLabel;
    }

    public JLabel getInfoLabel() {
        return infoLabel;
    }
}
