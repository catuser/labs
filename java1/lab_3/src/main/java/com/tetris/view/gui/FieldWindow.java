package com.tetris.view.gui;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

import com.tetris.Settings;
import com.tetris.model.Shape;

public class FieldWindow extends JComponent {
    private int widthN, heightN;
    private Shape.Type[][] field;

    public FieldWindow(int widthN, int heightN){
        this.widthN = widthN;
        this.heightN = heightN;
        field = createField(widthN, heightN);
    }

    private Shape.Type[][] createField(int width, int height){
        Shape.Type[][] field = new Shape.Type[height][width];
        for(int i = 0; i < height; i++)
            Arrays.fill(field[i], Shape.Type.NoShape);
        return field;
    }

    public void set(int i, int j, Shape.Type type){
        field[i][j] = type;
    }

    private Color getColor(Shape.Type type){
        if(type == Shape.Type.NoShape)
            return Color.gray;
        return new Color(Integer.MAX_VALUE / (type.ordinal() * type.ordinal() + 1)
                + type.ordinal() * type.ordinal() * 1111);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(widthN * getSquareSize() + 20, heightN * getSquareSize() + 10);
    }

    private Shape.Type getType(int i, int j){
        return field[i][j];
    }

    private int getSquareSize(){
        return (Settings.WIDTH - 20) / widthN;
    }

    private int getFieldX(){
        return 10;
    }

    private int getFieldY(){
        return 10;
    }

    private void drawBackGround(Graphics g){
        g.setColor(Color.black);
        g.fillRect(0, 0,
                widthN * getSquareSize() + 20, heightN * getSquareSize() + 20);
        g.setColor(Color.gray);
        g.fillRect(getFieldX(), getFieldY(),
                widthN * getSquareSize(), heightN * getSquareSize());
    }

    private void drawGrid(Graphics g) {
        g.setColor(Color.black);
        for(int i = 0; i < heightN; i++)
            g.drawLine(getFieldX(), getFieldY() + i * getSquareSize(),
                    getFieldX() + getSquareSize() * widthN, getFieldY() + i * getSquareSize());
        for (int j = 0; j < widthN; j++)
            g.drawLine(getFieldX() + j * getSquareSize(), getFieldY(),
                    getFieldX() + getSquareSize() * j, getFieldY() + getSquareSize() * heightN);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawBackGround(g);
        for(int i = 0; i < heightN; i++) {
            for (int j = 0; j < widthN; j++) {
                if(getType(i, j) != Shape.Type.NoShape) {
                    g.setColor(getColor(getType(i, j)));
                    g.fillRect(getFieldX() + j * getSquareSize(), getFieldY() + i * getSquareSize(), getSquareSize(), getSquareSize());
                }
            }
        }
        drawGrid(g);
    }
}
