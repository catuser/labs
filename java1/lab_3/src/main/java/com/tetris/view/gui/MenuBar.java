package com.tetris.view.gui;

import com.tetris.Settings;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class MenuBar extends JMenuBar{
    private Menu menu;
    private About about;

    public MenuBar() {
        super();
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(0, Settings.WIDTH / 8, 7, Settings.WIDTH / 8));

        menu = new Menu();
        add(menu, BorderLayout.WEST);

        about = new About();
        add(about, BorderLayout.EAST);
    }

    public Menu getMenu(){
        return menu;
    }

    public About getAbout(){
        return about;
    }
}
