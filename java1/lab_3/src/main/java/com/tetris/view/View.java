package com.tetris.view;

public interface View {
    void startNewGame();
    void reset();
    void showHighScores();
    void exit();
    void showAbout();
    void updateLoop();
}
