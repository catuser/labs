package com.tetris.view.gui;
import com.tetris.controller.Controller;
import com.tetris.model.GameLogic;
import com.tetris.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainPanel extends JPanel {
    private Controller controller;
    private GameLogic logic;

    private MenuBar menuBar;
    private JLabel infoLabel, scoreLabel, timeLabel;
    private LabelPanel labelPanel;
    private FieldWindow fieldWindow;

    public MainPanel(Controller controller, GameLogic logic){
        super();
        this.controller = controller;
        this.logic = logic;

        setLayout(new BorderLayout());
        setFocusable(true);
        requestFocus();
        addKeyListener(pressToStartListener());

        labelPanel = new LabelPanel();
        add(labelPanel, BorderLayout.SOUTH);

        infoLabel = labelPanel.getInfoLabel();
        scoreLabel = labelPanel.getScoreLabel();
        timeLabel = labelPanel.getTimeLabel();

        fieldWindow = new FieldWindow(logic.WIDTH, logic.HEIGHT);
        add(fieldWindow, BorderLayout.CENTER);

        menuBar = new MenuBar();
        menuBar.getMenu().getNewGameItem().addActionListener(e -> controller.onReset());
        menuBar.getMenu().getExitItem().addActionListener(e -> controller.onExit());
        menuBar.getMenu().getHighScoresItem().addActionListener(e -> controller.onHighScores());
        menuBar.getAbout().addActionListener(e -> controller.onAbout());
        add(menuBar, BorderLayout.NORTH);
    }

    private KeyListener pressToStartListener(){
        return new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    controller.onStart();
                    removeKeyListener(this);
                }
            }
        };
    }

    private void removeKeyListeners(KeyListener[] listeners){
        for(KeyListener listener: listeners)
            removeKeyListener(listener);
    }

    public void startNewGame(){
        addKeyListener(new MyKeyAdapter());
        infoLabel.setText("");
    }

    public void reset(){
        infoLabel.setText("<html><u>Press \"Enter\" to start");
        scoreLabel.setText("");
        timeLabel.setText("");

        BorderLayout layout = (BorderLayout)getLayout();
        remove(layout.getLayoutComponent(BorderLayout.CENTER));
        add(fieldWindow, BorderLayout.CENTER);
        add(labelPanel, BorderLayout.SOUTH);
        revalidate();

        removeKeyListeners(getKeyListeners());
        addKeyListener(pressToStartListener());
    }

    private boolean isAboutPressed = false;
    public void showAbout(){
        isAboutPressed = true;
        JOptionPane.showMessageDialog(this,
                "<html><h2>About</h2>Press \"space\" to move a shape down. \n Speed increases from 1 to 10.");
        isAboutPressed = false;
        requestFocus();
    }

    public void showHighScores(){
        BorderLayout layout = (BorderLayout)getLayout();
        remove(layout.getLayoutComponent(BorderLayout.CENTER));
        remove(layout.getLayoutComponent(BorderLayout.SOUTH));
        add(new JScrollPane(new HighScoresTable(logic.getHighScores())), BorderLayout.CENTER);
        revalidate();
    }

    public void sync() {
        if(logic.isGameOver()){
            infoLabel.setText("<html><u>Game Over");
        }
        if(logic.isStart()) {
            scoreLabel.setText("<html><b>Score: " + logic.getScore() + " ");
            timeLabel.setText(" Time: " + logic.getGameTime() + " s");
        }
        updateFieldWindowData();
        repaint();
    }

    @Override
    protected void paintBorder(Graphics g) {
        super.paintBorder(g);
        if(isAboutPressed)
            menuBar.getAbout().setBorder(true);
        else
            menuBar.getAbout().setBorder(false);
    }

    private int[][] coords = new int[4][2];

    private void updateFieldWindowData(){
        for(int i = 0; i < logic.HEIGHT; i++)
            for(int j = 0; j < logic.WIDTH; j++)
                fieldWindow.set(i, j, logic.getFieldCellType(j, i));

        for(int[] coord: logic.getFreshShapeCoords(coords)) {
            if(coord[1] >= 0)
                fieldWindow.set(coord[1], coord[0], logic.getFreshShapeType());
        }
    }

    class MyKeyAdapter extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent e) {
            super.keyPressed(e);
            switch(e.getKeyCode()){
                case KeyEvent.VK_LEFT:{
                    controller.onMoveLeft();
                    break;
                }
                case KeyEvent.VK_RIGHT:{
                    controller.onMoveRight();
                    break;
                }
                case KeyEvent.VK_UP:{
                    controller.onRotateLeft();
                    break;
                }
                case KeyEvent.VK_DOWN:{
                    controller.onRotateRight();
                    break;
                }
                case KeyEvent.VK_SPACE:{
                    controller.onMoveDown();
                    break;
                }
                default:
            }
        }
    }
}


