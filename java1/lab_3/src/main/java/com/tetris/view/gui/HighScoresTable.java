package com.tetris.view.gui;

import com.tetris.Settings;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class HighScoresTable extends JTable {
    public HighScoresTable(Integer[] scores) {
        super(getRowData(scores), new Object[]{"№", "Score"});
        setFocusable(false);
        setRowSelectionAllowed(false);

        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment( JLabel.CENTER );
        getColumn("№").setPreferredWidth(Settings.WIDTH / 4);
        getColumn("№").setCellRenderer(renderer);
        getColumn("Score").setPreferredWidth(Settings.WIDTH * 3 / 4);
        getColumn("Score").setCellRenderer(renderer);

        getTableHeader().setFont(new Font(Font.DIALOG, Font.BOLD, 30));

        setFont(new Font(Font.MONOSPACED, Font.BOLD, 20));
        setRowHeight(40);
    }

    @Override
    public boolean isCellEditable(int row,int column){
        return false;
    }

    private static Object[][] getRowData(Object[] scores){
        Object[][] data = new Object[scores.length][2];
        for(int i = 0; i < scores.length; i++){
            data[i][0] = i + 1;
            data[i][1] = scores[i];
        }
        return data;
    }
}
