package com.tetris.view.gui;

import com.tetris.utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class Menu extends JMenu {
    private JMenuItem newGameItem;
    private JMenuItem highScoresItem;
    private JMenuItem exitItem;

    public Menu() {
        super();

        setIcon(new ImageIcon(Utils.getScaledImage("/menu.png", 40, 40)));
        setMnemonic(KeyEvent.VK_X);

        newGameItem = new JMenuItem("New Game");
        newGameItem.setMnemonic(KeyEvent.VK_R);
        newGameItem.setFont(new Font(getFont().getName(), Font.BOLD, 20));
        newGameItem.setIcon(new ImageIcon(
                Utils.getScaledImage("/newgame.png", 20, 20)));
        add(newGameItem);

        highScoresItem = new JMenuItem("High Scores");
        highScoresItem.setFont(new Font(getFont().getName(), Font.PLAIN, 20));
        highScoresItem.setIcon(new ImageIcon(
                Utils.getScaledImage("/highscores.png", 20, 20)));
        add(highScoresItem);

        exitItem = new JMenuItem("Exit");
        exitItem.setFont(new Font(getFont().getName(), Font.PLAIN, 20));
        exitItem.setIcon(new ImageIcon(
                Utils.getScaledImage("/exit.png", 20, 20)));
        add(exitItem);
    }

    public JMenuItem getExitItem() {
        return exitItem;
    }

    public JMenuItem getHighScoresItem() {
        return highScoresItem;
    }

    public JMenuItem getNewGameItem() {
        return newGameItem;
    }
}
