package com.tetris.view.gui;

import com.tetris.utils.Utils;

import javax.swing.*;
import java.awt.*;

public class About extends JButton {
    public About() {
        super();
        setPreferredSize(new Dimension(52, 46));
        setContentAreaFilled(false);
        setFocusPainted(false);
        setBorderPainted(false);
        setBorder(BorderFactory.createLineBorder(new Color(122, 138, 153), 1));
        setBackground(new Color(163, 184, 204));
        setIcon(new ImageIcon(Utils.getScaledImage("/about.png", 40, 40)));
    }

    public void setBorder(boolean b){
        setBorderPainted(b);
        setOpaque(b);
    }
}
