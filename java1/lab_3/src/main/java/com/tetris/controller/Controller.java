package com.tetris.controller;

import com.tetris.model.GameLogic;
import com.tetris.view.View;

public class Controller{
    private GameLogic logic;
    private View view;

    public Controller(GameLogic logic){
        this.logic = logic;
    }

    public void setView(View view){
        this.view = view;
        view.updateLoop();
    }

    public void onMoveLeft(){
        logic.moveLeft();
    }

    public void onMoveRight(){
        logic.moveRight();
    }

    public void onMoveDown(){
        logic.moveDown();
    }

    public void onRotateLeft(){
        logic.rotateLeft();
    }

    public void onRotateRight(){
        logic.rotateRight();
    }

    public void onAbout(){
        view.showAbout();
    }

    public void onStart(){
        logic.start();
        view.startNewGame();
    }

    public void onReset(){
        logic.reset();
        view.reset();
    }

    public void onHighScores(){
        view.showHighScores();
    }

    public void onExit(){
        System.exit(0);
    }
}
