package com.tetris.model;

import java.util.*;
import java.util.prefs.Preferences;

public class HighScoresData {
    private Preferences prefs;

    public HighScoresData(){
        prefs = Preferences.userRoot().node("highscores");
    }

    public void add(int score){
        prefs.put("scores", prefs.get("scores", "") + score + " ");
    }

    public Integer[] getHighScores(){
        String[] strScores = prefs.get("scores", "").split(" ");
        Set<Integer> scores = new HashSet<>();
        for(String score: strScores)
            scores.add(Integer.parseInt(score));
        Integer[] res = scores.toArray(new Integer[scores.size()]);
        Arrays.sort(res, Comparator.reverseOrder());
        return res;
    }

    public void flush(){
        try {
            prefs.flush();
        }catch(Exception e){
            System.err.println(e.getLocalizedMessage());
        }
    }
}
