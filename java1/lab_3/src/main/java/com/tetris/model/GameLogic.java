package com.tetris.model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

public class GameLogic implements ActionListener{
    public final int WIDTH = 12, HEIGHT = 22;
    private GameField gameField;
    private Shape freshShape;
    private int x, y;

    private boolean gameOver = false, start = false;
    private HighScoresData highScoresData;
    private int score = 0;
    private float speed = 1;
    private long freshShapeTime = 0;
    private long startGameTime, gameTime;
    private Timer timer;

    public GameLogic(){
        highScoresData = new HighScoresData();
        gameField = new GameField(HEIGHT, WIDTH);
        timer = new Timer(400, this);
        newFreshShape();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        update();
    }

    public void start(){
        freshShapeTime = startGameTime = System.currentTimeMillis();
        timer.setDelay(400);
        timer.restart();
        start = true;
    }

    public void reset(){
        timer.stop();
        gameOver = false;
        start = false;
        score = 0;
        speed = 1;
        gameTime = 0;
        gameField.clear();
        newFreshShape();
    }

    private void update(){
        if(gameOver)
            return;

        if(gameField.hasCollision(freshShape, x, y + 1)) {
            timer.setDelay(getDelay(speed));
            timer.restart();

            for (int i = 0; i < 4; i++) {
                if (y - freshShape.getY(i) >= 0)
                    gameField.set(x + freshShape.getX(i), y - freshShape.getY(i), freshShape.getType());
                else {
                    gameOver = true;
                    gameTime = System.currentTimeMillis() - startGameTime;
                    if(score != 0) {
                        highScoresData.add(score);
                        highScoresData.flush();
                    }
                    return;
                }
            }

            int n = gameField.tryRemoveFilledLines();
            if(n > 0) {
                score += calcScore((int) (System.currentTimeMillis() - freshShapeTime), n);
                freshShapeTime = System.currentTimeMillis();

                speed = Math.max(0, Math.min(score / 100, 10));
            }

            newFreshShape();
        }
        else
            y++;
    }

    private int getDelay(float speed){
        return (int)(400 - speed * 30);
    }

    public int getGameTime(){
        return isGameOver() || !start ? (int)(gameTime / 1000): (int)(System.currentTimeMillis() - startGameTime) / 1000;
    }

    //time in milliseconds, n - a number of removed lines
    private int calcScore(int time, int n){
        final int MIN_SCORE_TIME = 20000, MAX_SCORE_TIME = 5000;
        return (int)Math.max(10, Math.min(100-90f/(MIN_SCORE_TIME - MAX_SCORE_TIME) * (time - MAX_SCORE_TIME), 100)) * n;
    }

    public int getScore(){
        return score;
    }

    public Integer[] getHighScores(){
        return highScoresData.getHighScores();
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isStart() {
        return start;
    }

    public void moveRight(){
        if(!gameField.hasCollision(freshShape, x + 1, y))
            x++;
    }

    public void moveLeft(){
        if(!gameField.hasCollision(freshShape, x - 1, y))
            x--;
    }

    public void moveDown(){
        timer.setInitialDelay(0);
        timer.setDelay(20);
        timer.restart();
    }

    public void rotateLeft(){
        Shape rotatedShape = freshShape.rotateLeft();
        if(!gameField.hasCollision(rotatedShape, x, y))
            freshShape = rotatedShape;
    }

    public void rotateRight(){
        Shape rotatedShape = freshShape.rotateRight();
        if(!gameField.hasCollision(rotatedShape, x, y))
            freshShape = rotatedShape;
    }

    public Shape.Type getFieldCellType(int x, int y){

        return gameField.get(x, y);
    }

    public int[][] getFreshShapeCoords(int[][] coords){
        for(int i = 0; i < 4; i++)
        {
            coords[i][0] = x + freshShape.getX(i);
            coords[i][1] = y - freshShape.getY(i);
        }

        return coords;
    }

    public Shape.Type getFreshShapeType(){
        return freshShape.getType();
    }

    private void newFreshShape(){
        if(freshShape == null)
            freshShape = new Shape(Shape.Type.random());
        else
            freshShape.setType(Shape.Type.random());
        x = WIDTH / 2;
        y = freshShape.getMinY() - 1;
    }
}
