package com.tetris.model;

import java.util.Random;

public class Shape {
    public enum Type{NoShape, ZShape, SShape, LineShape,
        TShape, SquareShape, LShape, MirroredLShape;

        private static Random random = new Random();
        static Type random(){
            return Type.values()[Math.abs(random.nextInt() % 7) + 1];
        }
    }

    private static final int[][][] coordsTable = new int[][][] {
        { { 0, 0 },   { 0, 0 },   { 0, 0 },   { 0, 0 } },
        { { 0, -1 },  { 0, 0 },   { -1, 0 },  { -1, 1 } },
        { { 0, -1 },  { 0, 0 },   { 1, 0 },   { 1, 1 } },
        { { 0, -1 },  { 0, 0 },   { 0, 1 },   { 0, 2 } },
        { { -1, 0 },  { 0, 0 },   { 1, 0 },   { 0, 1 } },
        { { 0, 0 },   { 1, 0 },   { 0, 1 },   { 1, 1 } },
        { { -1, -1 }, { 0, -1 },  { 0, 0 },   { 0, 1 } },
        { { 1, -1 },  { 0, -1 },  { 0, 0 },   { 0, 1 } }
    };
    private int[][] coords = new int[4][2];
    private Type type;

    private Shape(){}

    Shape(Type type) {
        setType(type);
    }

    public void setType(Type type){
        this.type = type;
        for(int i = 0; i < 4; i++)
            System.arraycopy(coordsTable[type.ordinal()][i], 0, coords[i], 0, 2);
    }

    public Type getType(){
        return type;
    }

    public int getX(int index){
        return coords[index][0];
    }

    public int getY(int index){
        return coords[index][1];
    }

    public int getMinY(){
        int min = getY(0);
        for(int i = 1; i < 4; i++)
            min = Math.min(min, getY(i));
        return min;
    }

    private void setCoords(int index, int x, int y){
        coords[index][0] = x;
        coords[index][1] = y;
    }

    public Shape rotateLeft(){
        if(type == Type.SquareShape)
            return this;

        Shape rotatedShape = new Shape();
        rotatedShape.type = type;
        for(int i = 0; i < 4; i++)
           rotatedShape.setCoords(i, getY(i), -getX(i));

        return rotatedShape;
    }

    public Shape rotateRight(){
        if(type == Type.SquareShape)
            return this;

        Shape rotatedShape = new Shape();
        rotatedShape.type = type;
        for(int i = 0; i < 4; i++)
            rotatedShape.setCoords(i, -getY(i), getX(i));

        return rotatedShape;
    }
}
