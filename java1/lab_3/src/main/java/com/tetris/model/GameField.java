package com.tetris.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameField {
    private final int height, width;
    private List<Shape.Type[]> field;

    public GameField(int height, int width){
        this.height = height;
        this.width = width;

        field = new ArrayList<>(height);
        for(int i = 0; i < height; i++) {
            field.add(createEmptyStr());
        }
    }

    public void set(int x, int y, Shape.Type type){
        field.get(y)[x] = type;
    }

    public Shape.Type get(int x, int y){
        return field.get(y)[x];
    }

    //returns a number of removed lines
    public int tryRemoveFilledLines(){
        int n = 0;
        for(int i = 0; i < getHeight(); i++) {
            boolean filled = true;
            for (int j = 0; j < getWidth(); j++) {
                if (get(j, i) == Shape.Type.NoShape) {
                    filled = false;
                    break;
                }
            }
            if(filled) {
                removeStr(i);
                n++;
            }
        }
        return n;
    }

    public boolean hasCollision(Shape shape, int x, int y){
        for(int i = 0; i < 4; i++){
            int curX = x + shape.getX(i);
            int curY = y - shape.getY(i);
            if(curX < 0 || curX >= getWidth() || curY >= getHeight()
                    || (curY >= 0 && get(curX, curY) != Shape.Type.NoShape))
                return true;
        }
        return false;
    }

    public void clear(){
        for(Shape.Type[] line: field)
            Arrays.fill(line, Shape.Type.NoShape);
    }

    private void removeStr(int y){
        field.remove(y);
        field.add(0, createEmptyStr());
    }

    public int getWidth() {
        return width;
    }

    public int getHeight(){
        return height;
    }

    private Shape.Type[] createEmptyStr(){
        Shape.Type[] str = new Shape.Type[width];
        Arrays.fill(str, Shape.Type.NoShape);
        return str;
    }
}
