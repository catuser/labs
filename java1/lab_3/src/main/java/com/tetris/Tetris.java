package com.tetris;

import com.tetris.controller.Controller;
import com.tetris.model.GameLogic;
import com.tetris.view.GView;
import com.tetris.view.View;

public class Tetris {
    public static void main(String[] args) {
        GameLogic gameLogic = new GameLogic();
        Controller controller = new Controller(gameLogic);
        View view = new GView(controller, gameLogic);
        controller.setView(view);
    }
}
