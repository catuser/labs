#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <iterator>

using namespace std;

namespace module {
	void sort_strings(list<string> &lines) {
		lines.sort();
	}
}

int main()
{
	ifstream in("input.txt");
	ofstream out("output.txt");
	list<string> lines;

	while (!in.eof())
	{
		string vrem;
		getline(in, vrem);
		lines.push_back(vrem);
	}
	using module::sort_strings;
	sort_strings(lines);
	copy(lines.begin(), lines.end(), ostream_iterator<string>(out, "\n"));
	//in.close();
	//out.close();
	return 0;
}