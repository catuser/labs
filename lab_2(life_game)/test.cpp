#include <gtest/gtest.h>
#include "../game.h"


TEST (game_tests, save_true)
{
    Game game;
    game.action(1, 1, true);
    game.action(2, 1, true);
    game.action(3, 1, true);
    game.save("test_save.txt");
    string str;
    ifstream save("test_save.txt");
    getline ( save, str, 'M' );
    string test_str1 = "1BCD";
    ASSERT_EQ(str, test_str1);
    save.close();
}

TEST (game_tests, save_false)
{
    Game game;
    game.action(1, 1, true);
    game.action(2, 2, true);
    game.action(3, 1, true);
    game.action(2, 3, true);
    game.save("test_save.txt");
    string str;
    ifstream save("test_save.txt");
    getline ( save, str, 'M' );
    string test_str1 = "1BCD";
    ASSERT_NE(str, test_str1);
}

TEST(game_tests, check_cell_status)
{
    Game game;
    History history;
    game.action(0, 1, 1, true);
    game.action(0, 2, 2, true);
    ASSERT_TRUE(game.check_cell_status(1, 1));
    ASSERT_TRUE(game.check_cell_status(2, 2));
    ASSERT_FALSE(game.check_cell_status(0, 0));
}

TEST(game_tests, action_logic)
{
    Game game;
    game.action(1, 1, true);
    game.action(1, 3, false);
    ASSERT_TRUE(game.get_logic_field().cells[1][1].alive);
    ASSERT_FALSE(game.get_logic_field().cells[3][1].alive);
    ASSERT_FALSE(game.get_logic_field().cells[1][3].alive);
}

TEST(game_tests, get_step_n)
{
    Game game;
    game.set_step_n(5);
    History history;
    ASSERT_EQ(game.get_step_n(), 5);
}

TEST(game_tests, load)
{
    Game game;
    ofstream load("test_load.txt");
    if (load.is_open())
    {
        load.write("1BCDM", 5);
        load.close();
    }
    game.load("test_load.txt");
    for (int x = 1; x < 4; x++)
    {
        ASSERT_TRUE(game.check_cell_status(x, 1));
    }
}

TEST (game_tests, game_over)
{
    Game game;
    ASSERT_FALSE(game.game_over());
    game.step(1);
    ASSERT_TRUE(game.game_over());
    game.action(1, 1, true);
    game.action(2, 1, true);
    game.action(3, 1, true);
    game.step(1);
    ASSERT_FALSE(game.game_over());
    Game game2;
    game2.action(0, 0, true);
    game2.action(0, 1, true);
    game2.action(1, 0, true);
    game2.action(1, 1, true);
    game2.step(2);
    ASSERT_TRUE(game2.game_over());
}

TEST (game_tests, reset)
{
    Game game;
    game.action(1, 1, true);
    game.action(2, 1, true);
    game.action(3, 1, true);
    game.step(2);
    int n = game.get_step_n();
    game.reset();
    ASSERT_NE(game.get_step_n(), n);
    ASSERT_EQ(game.get_step_n(), 0);
    ASSERT_FALSE(game.check_cell_status(1,1));
    ASSERT_FALSE(game.check_cell_status(2,1));
    ASSERT_FALSE(game.check_cell_status(3,1));
}

TEST (game_tests, step)
{
    Game game;
    game.action(1, 1, true);
    game.action(2, 1, true);
    game.action(3, 1, true);
    int n = game.get_step_n();
    game.step(1);
    ASSERT_FALSE(game.check_cell_status(1, 1));
    ASSERT_FALSE(game.check_cell_status(3, 1));
    ASSERT_TRUE(game.check_cell_status(2, 1));
    ASSERT_TRUE(game.check_cell_status(2, 0));
    ASSERT_TRUE(game.check_cell_status(2, 2));
    ASSERT_NE(game.get_step_n(), n);
    ASSERT_EQ(game.get_step_n(), 1);

}

TEST (game_tests, back)
{
    Game game;
    game.action(1, 1, true);
    game.action(2, 1, true);
    game.action(3, 1, true);
    game.step(4);
    int n = game.get_step_n();
    ASSERT_TRUE(game.check_cell_status(1, 1));
    ASSERT_TRUE(game.check_cell_status(2, 1));
    ASSERT_TRUE(game.check_cell_status(3, 1));
    game.step(-1);
    ASSERT_NE(game.get_step_n(), n);
    ASSERT_EQ(game.get_step_n(), 3);
    ASSERT_TRUE(game.check_cell_status(2, 1));
    ASSERT_TRUE(game.check_cell_status(2, 0));
    ASSERT_TRUE(game.check_cell_status(2, 2));
}