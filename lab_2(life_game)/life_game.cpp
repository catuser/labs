#include "life_game.h"

using namespace std;

void Cell::clear()
{
    alive = false;
}

void Cell::set()
{
    alive = true;
}

Cell::Cell()
{
    alive = false;
}


Field::Field()
{
    for (int j = 0; j < FIELD_HEIGHT; j++)
    {
        for (int i = 0; i < FIELD_WIDTH; i++)
        {
            cells[i][j].alive = false;
        }
    }
}


int History::get_step_n()
{
    return step_n;
}

bool History::match(int last_step, int x, int y)
{
    return (steps[last_step].cells[x][y].alive == steps[last_step - 1].cells[x][y].alive);
}

void History::action(int last_step, int x, int y, bool set)
{
    if(set)
        steps[last_step].cells[x][y].set();
    else
        steps[last_step].cells[x][y].clear();
}

void History::set_field(int last_step, Field f)
{
    steps[last_step] = f;
}

Field History::get_field(int last_step)
{
    return steps[last_step];
}

void History::p_back()
{
    steps.push_back(Field());
}

History::History()
{
    step_n = 0;
    steps.push_back(Field());
}


Logic::Logic()
{
    for (int i = 0; i < FIELD_WIDTH; i++)
    {
        for (int j = 0; j < FIELD_HEIGHT; j++)
        {
            field.cells[i][j].clear();
        }
    }
}

Field Logic::get_field()
{
    return field;
}

void Logic::set_field(Field f)
{
    field = f;
}

bool Logic::game_over(History h)
{
	bool match = true; //проверяет клетки данного шага с предыдущим
	bool is_empty = true; //проверяет все клетки на то, есть ли на них хотя бы один организм

    if (h.step_n != 0) //если уже был сделан один или больше шагов
    {
       
        for (int j = 0; (j < FIELD_HEIGHT) && is_empty; j++) //цикл прерывается, если находим организм на клетке
        {
            for (int i = 0; (i < FIELD_WIDTH) && is_empty; i++)
            {
                is_empty = !(field.cells[i][j].alive); //если на клетки нет организма, то true
            }
        }
        for (int j = 0; (j < FIELD_HEIGHT) && match; j++) //цикл прерывается, если находим отличное состояние клетки 
			                                              //этого шага с прошлым
        {
			for (int i = 0; (i < FIELD_WIDTH) && match; i++)
			{
				match = h.match(h.step_n, i, j); //сравниваем состояние клетки этого шага с предыдущим
			}
        }
		if (is_empty || match) //если всё поле пустое или предыдущее поле полностью повторяет поле этого шага
		{
			return true; //то игра завершится
		}
    }
    return false;
}

bool Logic::save(string filename)
{
	ofstream save(filename); //создал объект класс ofstream (создал файл для записи)
	if (save.is_open()) //если файл открыт
	{
		for (int j = 0; j < FIELD_HEIGHT; j++) //сначала записываю цифру ряда, если на одной или более клеток этого ряда есть организм
											  //после чего записываю буквы клеток данного ряда, на которых есть организм
										     //затем перехожу к следующему ряду, цифра которого будет j + 1
		{ 
			for (int i = 0; i < FIELD_WIDTH; i++)
			{
				if (field.cells[i][j].alive) //если на данной клетки есть организм,
				{                           
					save << j; //то записываю цифру ряда
					for (; i < FIELD_WIDTH; i++) //после чего записываю нужные буквы
					{
						if (field.cells[i][j].alive)
							save << (char)(i + (int)'A');
					}
				}
			}
		}
        save << 'M'; //последняя буква, не входящая в координаты поля, нужна, чтобы впоследствии знать,
		             //что дальше этой буквы нет нужной информации => дальше файл не читать
        save.close(); //закрываем файл
        return true;
    }
    save.close();
    return false;
}

void Logic::action(int x, int y, bool set)
{
    if(set)
        field.cells[x][y].set();
    else
        field.cells[x][y].clear();
}


Game::Game()
{
    Logic logic;
    History history;
}

void Game::save(string filename)
{
    logic.save(filename); //сохраняем состояние поля
}

bool Game::check_cell_status(int x, int y) //проверяет, есть ли организм на этой клетке
{
    return history.get_field(get_step_n()).cells[x][y].alive;
}

void Game::action(int last_step, int x, int y, bool set)
{
    history.action(last_step, x, y, set);
}

void Game::action(int x, int y, bool set)
{
    logic.action(x, y, set);
}

int Game::get_step_n()
{
    return history.get_step_n();
}

bool Game::load(string filename)
{
    char simbol; //переменная для временного хранения символов из файла
    ifstream load(filename); //открыл файл для чтения
    if (load.is_open()) //если файл открыт
    {
		reset(); //сбрасываю поле, как и все шаги. Начинаем history сначала
        int x, y; //координаты клеток, на которых надятся организмы
        while (true) //пока не дошли до конца файла (до символа M)
        {
			load >> simbol; //считал один символ из файла
			if ((simbol <= '9') && (simbol >= '0')) //если это цифра
				y = simbol - '0';                  //то в y сохраняем координату (FIELD_HEIGHT)
            if ((simbol < 'A') || (simbol > 'J')) //если в simbol лежит цифра, то
                load >> simbol;                  //считываем следующий символ, который будет буквой
            if (simbol == 'M') //если это конец файла
                break;        //то выходим из цикла
            x = simbol - 'A'; //в x сохраняем координату (FIELD_WIDTH)
			history.action(0, x, y, true); //помещяем организм  нужную клетку
			logic.action(x, y, true);
        }
        load.close(); //закрываем файл
        return true;
    }
    return false;
}

bool Game::game_over() //проверяем правила игры
{
    return logic.game_over(history);
}

void Game::reset()
{
	for (int j = 0; j < FIELD_HEIGHT; j++)
	{
		for (int i = 0; i < FIELD_WIDTH; i++) //убираем все организмы с поля
		{
			logic.action(i, j, false);
		}
	}
    history.set_field(0, logic.get_field()); //сбрасываем все сохраненные поля
    history.step_n = 0; //сбрасываем счетчик
}

void Game::step(int n)
{
    while (n > 0) //если делаем шаг вперед
    {
        history.p_back(); //создаем новое поле в векторе
        Field extra_f; //временная переменная для хранения ного поля
        int alive_counter; //счетчик клеток с живимы организмами
        for (int j = 0; j < FIELD_HEIGHT; j++) //проходим по всем клеткам
        {
            for (int i = 0; i < FIELD_WIDTH; i++)
            {
                alive_counter = 0; //сбрасываю счетчик
				//проверяю всех соседей данной клетки
				//если у данного соседа есть организм, то увеличиваю число счетчика на 1
                if (logic.get_field().cells[(i == 0) ? (FIELD_WIDTH - 1) : (i - 1)][(j == 0) ? (FIELD_HEIGHT - 1) : (j - 1)].alive)
                    alive_counter++;
                if (logic.get_field().cells[(i == 0) ? (FIELD_WIDTH - 1) : (i - 1)][j].alive)
                    alive_counter++;
                if (logic.get_field().cells[(i == 0) ? (FIELD_WIDTH - 1) : (i - 1)][(j == FIELD_HEIGHT - 1) ? 0 : (j + 1)].alive)
                    alive_counter++;
                if (logic.get_field().cells[(i == FIELD_WIDTH - 1) ? 0 : (i + 1)][(j == 0) ? (FIELD_HEIGHT - 1) : (j - 1)].alive)
                    alive_counter++;
                if (logic.get_field().cells[(i == FIELD_WIDTH - 1) ? 0 : (i + 1)][j].alive)
                    alive_counter++;
                if (logic.get_field().cells[(i == FIELD_WIDTH - 1) ? 0 : (i + 1)][(j == FIELD_HEIGHT - 1) ? 0 : (j + 1)].alive)
                    alive_counter++;
                if (logic.get_field().cells[i][(j == FIELD_HEIGHT - 1) ? 0 : (j + 1)].alive)
                    alive_counter++;
                if (logic.get_field().cells[i][(j == 0) ? (FIELD_HEIGHT - 1) : (j - 1)].alive)
                    alive_counter++;

                if (logic.get_field().cells[i][j].alive) //если у данной клетки есть организм
                {
                    if ((alive_counter < 2) || (alive_counter > 3)) //если у клетки менее 2 живых или более 3 живых соседей 
                        extra_f.cells[i][j].clear();               //то организм на этой клетке умирает
                    else //если у клетки 2 или 3 живых соседа
                        extra_f.cells[i][j].set(); //то организм остается на данной клетке
                }
                else //если у данной клетки нет организма
                {
                    if (alive_counter == 3) //если у клетки 3 живых соседа
                        extra_f.cells[i][j].set(); //то появляется новый организм на данной клетке
                    else //если у клетки нет ровно 3 живых соседей
                        extra_f.cells[i][j].clear(); //то организм не появляется
                }
            }
        }
        logic.set_field(extra_f); //сохраняем поле в logic
        history.step_n++; //добавляем к счетчику 1 шаг
        history.set_field(history.step_n, logic.get_field()); //сохраняем поле в history
        n--; //отнимаем от общего количество шагов один сделанный шаг
    }
    if ((n < 0) && (-n <= history.step_n)) //если делаем один шаг назад 
    {
        history.step_n += n; //отнимаем от счетчика один шаг
        logic.set_field(history.get_field(history.step_n)); //достаем из истории предыдущее поле
    }
}

Field Game::get_logic_field()
{
    return logic.get_field();
}

Field Game::get_history_field()
{
    return get_history_field();
}

void Game::set_step_n(int n)
{
    history.step_n = n;
}