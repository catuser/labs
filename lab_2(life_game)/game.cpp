#include "game.h"

void new_game( bool os)
{
	Game game;
	game.reset();
	while (!(game.game_over())) //���� �� �������� � ��� ��������� ���� ��������
	{
		string str; //� ��� ���������� ����� ������� ����� � INPUT
		if (os)
			system("clear"); //������ �����
		else 
			system("cls"); //������ �����

		/////////////////////////////////////////////////////////
		//���������� (������), ������� ����� ����� �� ����� ����
        cout << endl;
		cout << "m - emergency exit from the program" << endl;
		cout << "reset - reset program" << endl;
		cout << "back - return to previous step" << endl;
		cout << "save - save game" << endl;
		cout << "load - load game" << endl;
		cout << "set XY - put the body in a cage" << endl;
		cout << "step (N) - scroll the game forward by (N) or 1 steps" << endl;
		cout << "clear XY - clear the cage" << endl;
		//����� ����������
		/////////////////////////////////////////////////////////

		//����� ����
		cout << "\n";
		cout << "Step " << game.get_step_n() << endl;
		cout << "  A B C D E F G H I J \n";
		for (int y = 0; y < FIELD_HEIGHT; y++)
		{
			cout << y << ' ';
			for (int x = 0; x < FIELD_WIDTH; x++)
			{
				if (game.check_cell_status(x, y))
					cout << "* ";
				else
					cout << ". ";
			}
			cout << y << ' ';
			cout << '\n';
		}
		cout << "  A B C D E F G H I J \n";
		cout << "INPUT: "; //����� ����� ������ �������� �������
		getline(std::cin, str); //���� �������

		if (str[0] == 'm') //���������� ����� �� ���������, ���� ������� ���������� �� ����� 'm'
			exit(0); //���������� ����� �� ���������

		if (str == "reset") //reset
			game.reset(); //���������� ����� �������� ���� (��� ����� ������)


		if (str == "back") //back
			game.step(-1); //������������ �� ���� ��� �����

		if (str == "load") //load
		{
			cout << "enter the file name without extension" << endl; //���� ������ �� �������� �����
			cout << "for example: filename" << endl; //������ ����� ����� ������������ �����
			getline(std::cin, str); //���� ��� ����� ��� ����������
			game.load(str + ".txt");
		}

		if (str == "save") //save
		{
			cout << "enter the file name without extension" << endl; //���� ������ �� �������� �����
			cout << "for example: filename" << endl; //������ ����� ����� ������������ �����
			getline(std::cin, str); //���� ��� ����� ��� ����������
			game.save(str + ".txt");
		}


		//set XY
		if ((str[0] == 's') && (str[1] == 'e') && (str[2] == 't') && (str[3] == ' ')) {

            int x, y; //����������, ������� ������ ������������
            if ((str[4] >= 'A') && (str[4] <= 'J')) //�������� �� ������������ ����� ������ ���������� (X)
            {
                x = str[4] - 'A'; //���������� ���������� ����� int
            }

            if ((str[4] >= 'a') && (str[4] <= 'j')) //�������� �� ������������ ����� ������ ���������� (X)
            {
                x = (int) str[4] - (int) 'a'; //���������� ���������� ����� int
            }


            if ((str[5] <= '9') && (str[5] >= '0')) //�������� �� ������������ ����� ������ ���������� (Y)
                y = str[5] - '0'; //���������� ���������� ����� int

            if ((x >= 0) && (x < FIELD_WIDTH) && (y >= 0) && (y < FIELD_HEIGHT)) //�������������� ��������,
                //��� X, Y ����� � ������ �������
            {
                game.action(game.get_step_n(), x, y, true); //���������� ������ � history
                game.action(x, y, true); //���������� ������ � logic
            }
        }

		//step (N)
		if ((str[0] == 's') && (str[1] == 't') && (str[2] == 'e') && (str[3] == 'p'))
		{
			int n = 0; //���������� �����, ������� ������� ����� ������ ������� step
			int number; //����� N ����� �� str � int
			int quan = 5; //������������� 6 ������� �� str
			if (str.length() >= 6) //���� ������������ ���� ���������� �����, ��...
			{
				char simbol = str[quan];
				number = simbol - '0';
				if ((number > 9) || (number < 0)) //���� ����� ����� ���� �� ��������� �����, ��...
				{
					n = 1; //�� ������ ������ ���� ��� ������
				}
				else //���� ����� ����� ���� ��������� �����
				{
					for (int i = 0; ((number <= 9) && (number >= 0)) && (n <= 200);)
					{
						if (i == 0) //���������� ������ ������� ����� N (� ����� �������)
						{
							i++;
							n = number;
						}
						else //���� ����� ����� ������ ������ 9
						{
							n = n * 10 + number;
						}
						quan++; //���������, N > 9 ��� ���
						simbol = str[quan];
						number = simbol - '0';
					}
				}
			}
			else //���� ������������ �� ���� ���������� �����, �� 
				n = 1; //�� ������ ������ ���� ���
			game.step(n); //������ ����
		}

		//clear XY
		if ((str[0] == 'c') && (str[1] == 'l') && (str[2] == 'e') && (str[3] == 'a') && (str[4] == 'r') && (str[5] == ' '))
		{
			int x, y; //���������� ������ ������
			char simbol = str[6]; 
			if ((simbol >= 'A') && (simbol <= 'J')) //�������� �� ������������ ����� ������ ���������� (X)
			{
				x = simbol - 'A'; //���������� ���������� ����� int
			}
			if ((simbol >= 'a') && (simbol <= 'j')) //�������� �� ������������ ����� ������ ���������� (X)
			{
				x = simbol - 'a'; //���������� ���������� ����� int
			}
			simbol = str[7];
			if ((simbol <= '9') && (simbol >= '0')) //�������� �� ������������ ����� ������ ���������� (Y)
				y = simbol - '0'; //���������� ���������� ����� int
			if ((x >= 0) && (x < FIELD_WIDTH) && (y >= 0) && (y < FIELD_HEIGHT)) //�������������� ��������, 
				                                                                 //��� X, Y ����� � ������ �������
			{
				game.action(game.get_step_n(), x, y, false); //�������� ������ � history
				game.action(x, y, false); //�������� ������ � logic
			}
		}
	}
}