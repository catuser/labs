#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdio>

#define FIELD_HEIGHT 10
#define FIELD_WIDTH 10

using namespace std;

class Cell //����� ������
{
public:
    bool alive; //������ �����
    void clear(); //������ ���������� �������
    void set(); //������ ���������� �����
    Cell();
};

class Field //����� ����, ������� ������� �� ������
{
public:
    Cell cells[FIELD_HEIGHT][FIELD_WIDTH];
    Field();
};

class History
{
private:
    vector<Field> steps; //������ ���� Field
public:
    int step_n; //���������� �����
    int get_step_n(); //���������� �������� step_n
    bool match(int last_step, int x, int y); //��������� ��������� ������ ����� ��������� ����� � ����������
    void action(int last_step, int x, int y, bool set); //������� ��� ��������� �������� (������� �� set)
    void set_field(int last_step, Field f); //��������� ���� � �������
    Field get_field(int last_step); //�������� ������ ���� �� �������
    void p_back(); //��������� ���� � ����� �������
    History();
};

class Logic
{
private:
    Field field;
public:
    Logic();
    Field get_field(); //�������� ���� �� logic
    void set_field(Field f); //�������� ����
    bool game_over(History h); //��������� ������� ����
    bool save(string filename); //���������� � ��������� ���� ����
    void action(int x, int y, bool set); //���������� ��� ����������� ��������� (������� �� set)
};

class Game //�����, ����� ������� ���������� ��������������
{
private:
    Logic logic;
    History history;

public:
    //Field field;

    Game();
    void save(string filename); //���������� ����
    bool check_cell_status(int x, int y); //���������, ���� �� �������� � ������ ������
    void action(int last_step, int x, int y, bool set); //���������� ��� ����������� ��������� (������� �� set) (history)
    void action(int x, int y, bool set); //���������� ��� ����������� ��������� (������� �� set) (logic)
    int get_step_n(); //���������� �������� step_n (�� ����� ���� ������ ���������)
    bool load(string filename); //�������� ���� �� �������� ���������
    bool game_over(); //��������� ������� ����
    void reset(); //���������� ����, ��� � ��� ����. �������� history �������
    void step(int n); //���������� ��� ���� �� ��������
    Field get_logic_field(); //��� �����
    Field get_history_field(); //���  �����
    void set_step_n(int step_n); //��� �����
};

