//
// Created by michail on 05.12.17.
//

#include <gtest/gtest.h>
#include "../hash_table.h"


TEST (hash_table_tests, Swap) {
    HashTable A(1), B(1);
    A.insert("aaaa", {20, 40});
    B.insert("bbbb", {10, 60});
    B.swap(A);
    ASSERT_EQ(A["bbbb"].age, 10);
    ASSERT_EQ(A["bbbb"].weight, 60);
    ASSERT_EQ(B["aaaa"].age, 20);
    ASSERT_EQ(B["aaaa"].weight, 40);
}


TEST (hash_table_tests, Clear) {
    HashTable A;
    A.insert("aaaa", {20, 40});
    A.clear();
    ASSERT_EQ(A.size(), 0);
    ASSERT_TRUE(A.empty());
}

TEST (hash_table_tests, Erase) {
    HashTable A;
    A.insert("aaaa",{20, 40});
    ASSERT_TRUE(A.contains("aaaa"));
    ASSERT_TRUE(A.erase("aaaa"));
    ASSERT_EQ(A.size(), 0);
    ASSERT_TRUE(A.empty());
    ASSERT_FALSE(A.contains("aaaa"));
    ASSERT_FALSE(A.erase("bbbb"));
}

TEST (hash_table_tests, Insert) {
    HashTable A (1);
    HashTable B(A);
    A.insert("aaaa", {20, 40});
    ASSERT_LT(B.size(), A.size());
    ASSERT_EQ(B.size() + 1, A.size());
    ASSERT_GT(A.amount(), 1);
    ASSERT_EQ(A.size(), 1);
    ASSERT_FALSE(A.empty());
    ASSERT_TRUE(A.contains("aaaa"));
}

TEST (hash_table_tests, Contains) {
    HashTable A;
    A.insert("aaaa", {20, 40});
    ASSERT_TRUE(A.contains("aaaa"));
    A.erase("aaaa");
    ASSERT_FALSE(A.contains("aaaa"));
}

TEST (hash_table_tests, At) {
    HashTable A;
    Value a = {20, 40};
    Value b = {0, 0};
    A.insert("aaaa", a);
    ASSERT_THROW(A.at("bbbb"), out_of_range);
    ASSERT_EQ(a, A["aaaa"]);
    ASSERT_EQ(a, A.at("aaaa"));
    ASSERT_EQ(b, A["bbbb"]);
}

TEST (hash_table_tests, Size) {
    HashTable A;
    HashTable B(1);
    ASSERT_EQ(A.size(), 0);
    ASSERT_EQ(B.size(), 0);
    A.insert("aaaa", {20, 40});
    ASSERT_EQ(A.size(), 1);
    B = A;
    ASSERT_EQ(A.size(), B.size());
}

TEST (hash_table_tests, Empty) {
    HashTable A;
    ASSERT_TRUE(A.empty());
    A.insert("aaaa", {20, 40});
    HashTable B(A);
    ASSERT_EQ(B["aaaa"].age, 20);
    ASSERT_EQ(B["aaaa"].weight, 40);
    ASSERT_FALSE(A.empty());
    ASSERT_EQ(A.empty(), B.empty());
}

TEST (hash_table_tests, Operators) {
    HashTable A(1), B(1);
    A.insert("aaaa", {20,40});
    ASSERT_GT(A.amount(), B.amount());
    ASSERT_NE(B, A);
    B = A;
    ASSERT_EQ(A.amount(), B.amount());
    ASSERT_EQ(B, A);
}