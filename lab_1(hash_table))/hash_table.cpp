#include"hash_table.h"

unsigned HashTable::get_hash(const string &key) const {

    hash<string> hash_s;
    return unsigned(hash_s(key) % table_size);
}


bool HashTable::time_to_rebuild() {

    return ((double) number_of_filled_cells / (double) table_size) > 0.7;
}


unsigned HashTable::new_size() {

    return table_size * 3;
}


void HashTable::rebuild() {

    unsigned old_size = table_size;
    table_size = new_size();
    auto *new_table = new List[table_size];

    for (unsigned i = 0; i < old_size; ++i)
        for (auto s : table[i]) {
            int position = 0;
            position = get_hash(s.key); //считает хеш
            new_table[position].push_back(s);
        }
    delete[]table;
    table = new_table;
}


void HashTable::clear_table() {

    for (unsigned i = 0; i < table_size; ++i) {
        table[i].clear();
    }
}


HashTable::HashTable(unsigned n = 100) : table_size(n), number_of_filled_cells(0), starting_value(n) {

    table = new List[n];
}



HashTable::HashTable() : table_size(100), number_of_filled_cells(0), starting_value(100) {

    table = new List[100];
}


HashTable::~HashTable() {

    clear_table();
    delete[]table;
}


HashTable::HashTable(const HashTable &b) {

    this->number_of_filled_cells = b.number_of_filled_cells;
    this->table_size = b.table_size;
    this->starting_value = b.starting_value;
    this->table = new List[this->starting_value];
    for (unsigned i = 0; i < b.table_size; ++i)
        this->table[i] = b.table[i];
}


void HashTable::swap(HashTable &b) {

    unsigned help_variable;// временная переменная
    List *new_table; // временный массив List

    //swap значения table_size
    help_variable = table_size;
    table_size = b.table_size;
    b.table_size = help_variable;

    //swap значения starting_value
    help_variable = starting_value;
    starting_value = b.starting_value;
    b.starting_value = help_variable;

    //swap значения number_of_filled_cells
    help_variable = number_of_filled_cells;
    number_of_filled_cells = b.number_of_filled_cells;
    b.number_of_filled_cells = help_variable;

    //swap массив
    new_table = table;
    table = b.table;
    b.table = new_table;
}


HashTable &HashTable::operator=(const HashTable &b) {

    if (&b == this)
        return *this;
    delete[] table;
    table = new List[b.table_size];
    for (unsigned i = 0; i < b.table_size; ++i) {
        table[i] = b.table[i];
    }
    number_of_filled_cells = b.number_of_filled_cells;
    starting_value = b.starting_value;
    table_size = b.table_size;
    return *this;
}


void HashTable::clear() {

    //удаляем Table
    clear_table();
    delete[]table;

    //возвращаем стартовые значения
    number_of_filled_cells = 0;
    table_size = starting_value;
    table = new List[starting_value];
}

bool HashTable::erase(const string &k) {

    unsigned position = 0;
    position = get_hash(k); // считаю хеш
    if (!number_of_filled_cells) // проверка Table, если пустая, то false
        return false;
    List::iterator s;
    for (s = table[position].begin(); s != table[position].end(); ++s)
        if (s->key == k) { //если ключи совпадает, то удаляем этот елемент
            table[position].erase(s);
            (this->number_of_filled_cells)--;
            return true;
        }
    return false;
}

bool HashTable::insert(const string &k, const Value &v) {

    unsigned position = 0;
    position = get_hash(k); //считает хеш
    table[position].push_front({v, k});
    ++number_of_filled_cells;
    if (time_to_rebuild()) //если нужно пересоздать Table, то
    {
        rebuild();
    }
    return true;
}


bool HashTable::contains(const string &k) const {

    unsigned position = 0;
    position = get_hash(k); // считаю хеш
    if (!number_of_filled_cells) // проверка Table, если пустая, то false
        return false;
    for (auto s : table[position])
        if (s.key == k)
            return true;
    return false;
}


Value &HashTable::operator[](const string &k) {

    unsigned position = 0;
    position = get_hash(k); // считает хеш
    for (auto &s : table[position])
        if (s.key == k)
            return s.value;
    table[position].push_front({{0, 0}, k});
    return table[position].begin()->value;
}


Value &HashTable::at(const string &k) {

    unsigned position = 0;
    position = get_hash(k); // считает хеш
    for (auto &s : table[position])
        if (s.key == k)
            return s.value;
    throw  out_of_range("Bad key");
}


size_t HashTable::size() const {

    return number_of_filled_cells;
}

unsigned HashTable::amount() {
    return table_size;
}

bool HashTable::empty() const {

    return !number_of_filled_cells;
}


bool operator==(const HashTable &a, const HashTable &b) {

    if (a.number_of_filled_cells != b.number_of_filled_cells) //если колличество number_of_filled_cells разное, то
        return false;
    bool a_is_less = a.table_size < b.table_size;

    if (a_is_less) {
        //ищем каждый элемент
        for (unsigned i = 0; i < a.table_size; ++i) {
            for (auto s : a.table[i]) {
                if (!(b.contains(s.key)))
                    return false;
            }
        }
    }
    else {
        for (unsigned i = 0; i < b.table_size; ++i) {
            for (auto s : b.table[i]) {
                if ((!a.contains(s.key)))
                    return false;
            }
        }
    }
    return true;

}


bool operator!=(const HashTable &a, const HashTable &b) {
    return !(a == b);
}

bool operator==(const Value &a, const Value &b) {
    return a.age == b.age && a.weight == b.weight;
}
