#include <iostream>
#include <gtest/gtest.h>
//#include "hash_table.h"

int main(int argc, char* argv[]) {
    std::cout << "running hash_table_tests" << std::endl;

    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();

    return 0;
}
