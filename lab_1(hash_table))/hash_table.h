#include <iostream>
#include <string>
#include <list>
#include <exception>

using namespace std;

struct Value {
    unsigned age;
    unsigned weight;
};

struct Data {
    Value value;
    string key;
};

typedef list<Data> List;

class HashTable
{
private:
    unsigned table_size; // размер массива List
    unsigned number_of_filled_cells;// колличество заполненных ячеек
    unsigned starting_value;//начальное значение table_size
    List * table;// массив List
    unsigned get_hash(const string &k) const;//считает хеш

    bool time_to_rebuild(); // требуется ли пересоздание Table или нет

    unsigned new_size();// новый размер table_size

    void rebuild();// пересоздание table

    void clear_table();// очищает table

public:

    HashTable(unsigned n);

    HashTable();

    HashTable(const HashTable& b);

    ~HashTable();

    void swap(HashTable& b); //swap HashTables

    HashTable& operator=(const HashTable& b); // операция присваивания

    void clear();//чистит HashTable

    bool erase(const string& k); // уничтожает элемент

    bool insert(const string& k, const Value& v); // добавление элемента

    bool contains(const string& k) const;// проверяет, есть ли в table элемент

    Value& operator[](const string& k);

    Value& at(const string& k);

    size_t size() const;// возвращает значение number_of_filled_cells

    bool empty() const; // пустой?

    friend bool operator==(const HashTable & a, const HashTable & b);

    friend bool operator!=(const HashTable & a, const HashTable & b);

    unsigned amount(); // возвращает значение table_size
};

bool operator==(const HashTable & a, const HashTable & b);

bool operator!=(const HashTable & a, const HashTable & b);

bool operator==(const Value & a, const Value & b);
